# import the necessary packages
from imutils.perspective import four_point_transform
from imutils import contours
import imutils
import cv2
import numpy
import glob
import uuid
from sys import argv
# define the dictionary of digit segments so we can identify
# each digit on the thermostat
DIGITS_LOOKUP = {
    (1, 1, 1, 0, 1, 1, 1): 0,
    (0, 0, 1, 0, 0, 1, 0): 1,
    (1, 0, 1, 1, 1, 0, 1): 2,
    (1, 0, 1, 1, 0, 1, 1): 3,
    (0, 1, 1, 1, 0, 1, 0): 4,
    (1, 1, 0, 1, 0, 1, 1): 5,
    (1, 1, 0, 1, 1, 1, 1): 6,
    (1, 1, 1, 0, 0, 1, 0): 7,
    (1, 1, 1, 1, 1, 1, 1): 8,
    (1, 1, 1, 1, 0, 1, 1): 9
}
script_name, path = argv
#path = "tonometer3/1.jpg"
img = cv2.imread(path)

gray = cv2.cvtColor(img, cv2.COLOR_BGRA2GRAY)
cv2.imwrite("dsp/gray.jpg", gray, None)

blur = cv2.GaussianBlur(gray,(5,5), cv2.BORDER_DEFAULT)
cv2.imwrite("dsp/blur.jpg", blur, None)

# plot all the images and their histograms
thresh = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY_INV,31,3)

cv2.imwrite("dsp/thresh.jpg", thresh, None)

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
morphology = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
cv2.imwrite("dsp/morphology.jpg", morphology, None)

# find contours in the edge map, then sort them by their
# size in descending order
cnts = cv2.findContours(morphology.copy(), cv2.RETR_LIST,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
displayCnt = None

h = 0
w = 0
x = 0
y = 0
horizontal_display = None
# loop over the contours
for c in cnts:
	# approximate the contour
	peri = cv2.arcLength(c, True)
	approx = cv2.approxPolyDP(c, 0.02 * peri, True)

	(x, y, w, h) = cv2.boundingRect(c)
	# roi = morphology[y:y + h, x:x + w]
	# cv2.imwrite('wraped/' + str(uuid.uuid4()) + "roi.jpg", roi, None)
	# if the contour has four vertices, then we have found
	# the thermostat display
	if len(approx) == 4:
		if 2.9 * h <= w and w <= h * 3.4: #горизонтальный дисплей
			horizontal_display = True
			displayCnt = approx
			break
		if w < h * 0.7:#вертикальный дисплей
			horizontal_display = False
			displayCnt = approx
			break

warped = img[y:y+h, x:x+w]
# warped = four_point_transform(img, displayCnt.reshape(4, 2))

# warped = imutils.resize(warped, height=580)
cv2.imwrite("dsp/warped.jpg", warped, None)

# cv2.imwrite("dsp/warped.jpg", warped, None)
# load the example image
image = warped

gray = cv2.cvtColor(image, cv2.COLOR_BGRA2GRAY)
cv2.imwrite("gray.jpg", gray, None)

blurred = cv2.GaussianBlur(gray, (7, 7), cv2.BORDER_DEFAULT)
cv2.imwrite("blurred.jpg", blurred, None)

adaptiveThresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 171, 10)
cv2.imwrite("adaptiveThresh.jpg", adaptiveThresh, None)

# ret, thresh = cv2.threshold(adaptiveThresh, 150, 255, cv2.THRESH_BINARY)
# cv2.imwrite("threshold.jpg", thresh, None)

kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
morphology = cv2.morphologyEx(adaptiveThresh, cv2.MORPH_CLOSE, kernel)
cv2.imwrite("morphologyEx.jpg", morphology, None)

kernel =  numpy.ones(( 13, 13), numpy.uint8)
dilated =  cv2.dilate(morphology, kernel, iterations =  2)
cv2.imwrite("dilated.jpg", dilated, None)

# find contours in the thresholded image, then initialize the
# digit contours lists
cnts = cv2.findContours(dilated.copy(), cv2.RETR_LIST,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
digitCnts = []

output = image

dsp_h = numpy.size(image, 0)
dsp_w = numpy.size(image, 1)
# loop over the digit area candidates
for c in cnts:
	# compute the bounding box of the contour
	(x, y, w, h) = cv2.boundingRect(c)

	# if the contour is sufficiently large, it must be a digit
	if h >= 250 and  h > w :
		digitCnts.append(c)

# sort the contours from left-to-right, then initialize the
# actual digits themselves
# digitCnts = contours.sort_contours(digitCnts,
# 	method="left-to-right")[0]

upper_pressure_cnts = []
lower_pressure_cnts = []
pulse_cnts = []

#разбиваем по горизонтали на показатели
def clusteringByParameterHorizontalDisplay():
	for cnt in digitCnts:
		x = cv2.boundingRect(cnt)[0]
		#если левая часть, то верхнее давление
		if x <  dsp_w / 2.0:
			upper_pressure_cnts.append(cnt)
		else:
			# если правая часть, то нижнее давление
			lower_pressure_cnts.append(cnt)

def clusteringByParameterVerticalDisplay():
	for cnt in digitCnts:
		y = cv2.boundingRect(cnt)[1]
		# если верхняя часть, то верхнее давление
		if y < 1.0 / 4.0 * dsp_h:
			upper_pressure_cnts.append(cnt)
		else:
			# если средняяя часть, то нижнее давление
			if y < 1.0 / 2.0 * dsp_h:
				lower_pressure_cnts.append(cnt)
			# иначе нижняя часть, пульс
			else:
				pulse_cnts.append(cnt)

if horizontal_display:
	clusteringByParameterHorizontalDisplay()
else:
	clusteringByParameterVerticalDisplay()

#сортируем контуры цифр показателей слева направо
if len(upper_pressure_cnts) > 0:
	upper_pressure_cnts = contours.sort_contours(upper_pressure_cnts,
		method="left-to-right")[0]
if len(lower_pressure_cnts) > 0:
	lower_pressure_cnts = contours.sort_contours(lower_pressure_cnts,
		method="left-to-right")[0]
if len(pulse_cnts) > 0:
	pulse_cnts = contours.sort_contours(pulse_cnts,
		method="left-to-right")[0]


def is_one(roi, w, h):
	topSegROI = roi[h // 6: h // 3, w // 4: int(w * 3.0 / 4.0)]
	lowSegROI = roi[int(h / 6.0 + h / 2.0): int(h / 3.0 + h / 2.0), w // 4: int(w * 3.0 / 4.0)]
	totalTop = cv2.countNonZero(topSegROI)
	totalLow = cv2.countNonZero(lowSegROI)
	areaTop = (h / 6.0 -  h / 3.0) * ( w / 4.0 - w * 3.0 / 4.0)
	areaLow = areaTop
	if (areaTop > 0
			and (totalTop / float(areaTop) > 0.7)
			and (totalLow / float(areaLow) > 0.7)):
		return True;
	else:
		return False

def cnts_to_digits(digitCnts):
	digits = []
	# loop over each of the digits
	for c in digitCnts:
		# extract the digit ROI
		(x, y, w, h) = cv2.boundingRect(c)
		roi = dilated[y:y + h, x:x + w]
		cv2.imwrite("rois/" + str(uuid.uuid4()) + "roi.jpg", roi, None)
		#проверка на единицу

		if is_one(roi, w, h):
			digit = 1
			digits.append(digit)
			cv2.rectangle(output, (x, y), (x + w, y + h), (0, 255, 0), 1)
			cv2.putText(output, str(digit), (x - 10, y - 10),
			cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
			continue
		# compute the width and height of each of the 7 segments
		# we are going to examine
		(roiH, roiW) = roi.shape
		(dW, dH) = (int(roiW * 0.3), int(roiH * 0.2))
		dHC = int(roiH * 0.05)

		# define the set of 7 segments
		segments = [
			((dW, 0), (w - dW, dH)),	# top
			((0, dH), (dW, 2 * dH)),	# top-left
			((w - dW, dH), (w, 2 * dH)),	# top-right
			((dW, 2 * dH) , (w - dW, 3 * dH)), # center
			((0, 3 * dH), (dW, 4 * dH)),	# bottom-left
			((w - dW, 3 * dH), (w, h - dH)),	# bottom-right
			((dW, h - dH), (w - dW, h))	# bottom
		]
		on = [0] * len(segments)

		# loop over the segments
		for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
			# extract the segment ROI, count the total number of
			# thresholded pixels in the segment, and then compute
			# the area of the segment
			segROI = roi[yA:yB, xA:xB]
			total = cv2.countNonZero(segROI)
			area = (xB - xA) * (yB - yA)
			if area > 0 and (total / float(area) > 0.5):
				on[i]= 1

	# lookup the digit and draw it on the image
		if tuple(on) in DIGITS_LOOKUP:
			digit = DIGITS_LOOKUP[tuple(on)]
			digits.append(digit)
			cv2.rectangle(output, (x, y), (x + w, y + h), (0, 255, 0), 1)
			cv2.putText(output, str(digit), (x - 10, y - 10),
			cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
	return digits

digits_upper_pressure = cnts_to_digits(upper_pressure_cnts)
digits_lower_pressure = cnts_to_digits(lower_pressure_cnts)
digits_pulse = cnts_to_digits(pulse_cnts)

if len(digits_upper_pressure) == 0:
	digits_upper_pressure.append(0)
if len(digits_lower_pressure) == 0:
	digits_lower_pressure.append(0)
if len(digits_pulse) == 0:
	digits_pulse.append(0)

print(*digits_upper_pressure)
print(*digits_lower_pressure)
print(*digits_pulse)

cv2.imwrite("input.jpg", image, None)
cv2.imwrite("output.jpg", output, None)

cv2.waitKey(0)